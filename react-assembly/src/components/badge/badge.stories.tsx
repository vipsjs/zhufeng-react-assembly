import React from "react";
import { select, text } from "@storybook/addon-knobs";
import Badge, { BadgeProps } from "./badge";
import { badgeColor } from "../shared/styles";

type selectType = "positive" | "negative" | "neutral" | "warning" | "error";

export const knobsBadge = () => (
  <Badge
    status={select<BadgeProps["status"]>(
      "status",
      Object.keys(badgeColor) as selectType[],
      "neutral"
    )}
  >
    {text("children", "i am badge")}
  </Badge>
);
