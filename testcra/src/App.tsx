import React from "react";
import Button from "react-assembly/dist/stories/components/button";
import { GlobalStyle } from "react-assembly";
import "./App.css";

function App() {
  return (
    <div className="App">
      <GlobalStyle></GlobalStyle>
      <Button appearance="primary">2222</Button>
    </div>
  );
}

export default App;
